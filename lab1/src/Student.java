


/**
 * Student is person
 */
public class Student extends Person {
	private long id;
	

	/**
	 * Initialize a new Student object.
	 * @param name is the name of the new Student.
	 * @param id is the id of the new Student.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}


	/**
	 * Compare student's by name and id.
	 * They are equal if the name matches.
	 * @param other is another Student to compare to this one.
	 * @return true if the name and id are same, false otherwise.
	 */
	public boolean equals(Student other) {
		if (other == null) return false;
		if ( other.getClass() != this.getClass() )
			return false;
		Student student = (Student) other;
		
		if ( this.getName().equalsIgnoreCase( student.getName() ) && id == student.id )
			return true;
		
		return false; 
		
	}
}
